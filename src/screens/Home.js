import React from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native';
import Logo from '../assets/img/logo.png';

const Home = props => {
  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <Image style={{width: 100, height: 100}} source={Logo} />
      <View style={{marginVertical: 20, alignItems: 'center'}}>
        <Text style={{fontSize: 20}}>Selamat Datang di ExCode</Text>
        <Text style={{fontSize: 12}}>Klik Scan Exam Untuk Memulai Ujian</Text>
      </View>
      <TouchableOpacity onPress={() => props.navigation.navigate('Camera')}>
        <View
          style={{
            backgroundColor: '#EE3B49',
            alignItems: 'center',
            justifyContent: 'center',
            borderRadius: 30,
            width: 200,
          }}>
          <Text style={{marginVertical: 8, color: '#FFF', fontSize: 16}}>
            Scan Exam
          </Text>
        </View>
      </TouchableOpacity>
    </View>
  );
};

export default Home;
