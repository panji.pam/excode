import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import {RNCamera} from 'react-native-camera';
import * as Animatable from 'react-native-animatable';
import SVGTorch from '../assets/svg/Torch';
import SVGTorchOff from '../assets/svg/TorchOff';
import QRMarker from '../assets/svg/QRMarker';
// import SvgUri from 'react-native-svg-uri';
// import Icon from "react-native-vector-icons/MaterialCommunityIcons";

class Camera extends Component {
  constructor(props) {
    super(props);
    this.camera = null;
    this.state = {
      camera: {
        type: RNCamera.Constants.Type.back,
        barcodeFinderVisible: true,
      },
      flashMode: RNCamera.Constants.FlashMode.off,
      flashOn: false,
    };
  }

  async togleFlash() {
    await this.setState({flashOn: !this.state.flashOn});
    if (this.state.flashOn) {
      this.setState({flashMode: RNCamera.Constants.FlashMode.torch});
    } else {
      this.setState({flashMode: RNCamera.Constants.FlashMode.off});
    }
  }

  onBarCodeRead(scanResult) {
    if (scanResult.data != null) {
      //alert(scanResult.data);
      // console.warn({data: scanResult.data});
      this.props.navigation.navigate('Detail', {ScanDetail: scanResult.data});
    }
    return;
  }

  makeSlideOutTranslation(translationType, fromValue) {
    return {
      from: {
        [translationType]: -5,
      },
      to: {
        [translationType]: fromValue,
      },
    };
  }

  async takePicture() {
    if (this.camera) {
      const options = {quality: 0.5, base64: true};
      const data = await this.camera.takePictureAsync(options);
      console.log(data.uri);
    }
  }

  pendingView() {
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: 'lightgreen',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Text>Waiting</Text>
      </View>
    );
  }
  render() {
    const {height, width} = Dimensions.get('window');
    const maskRowHeight = Math.round((height - 200) / 20);
    const maskColWidth = (width - 250) / 2;

    return (
      <View style={styles.container}>
        {/* <View style={styles.cameraView}> */}
        <RNCamera
          ref={ref => {
            this.camera = ref;
          }}
          barcodeFinderVisible={this.state.camera.barcodeFinderVisible}
          // barcodeFinderBorderColor="white"
          // barcodeFinderBorderWidth={2}
          defaultTouchToFocus
          flashMode={this.state.flashMode}
          mirrorImage={false}
          onBarCodeRead={this.onBarCodeRead.bind(this)}
          style={styles.cameraView}
          type={this.state.camera.type}>
          <View style={styles.maskOutter}>
            <View
              style={[{flex: maskRowHeight}, styles.maskRow, styles.maskFrame]}
            />
            <View style={[{flex: 40}, styles.maskCenter]}>
              <View style={[{width: maskColWidth}, styles.maskFrame]} />
              <View style={styles.maskInner}>
                <QRMarker />
                <Animatable.View
                  style={styles.scanBar}
                  direction="alternate-reverse"
                  iterationCount="infinite"
                  duration={2000}
                  easing="linear"
                  animation={this.makeSlideOutTranslation('translateY', -295)}
                />
              </View>
              <View style={[{width: maskColWidth}, styles.maskFrame]} />
            </View>
            <View
              style={[{flex: maskRowHeight}, styles.maskRow, styles.maskFrame]}
            />
          </View>
        </RNCamera>
        <View
          style={{
            flex: 1,
            position: 'absolute',
            alignItems: 'center',
            flexDirection: 'row',
            margin: 20,
          }}>
          <View
            style={{
              flexDirection: 'row',
              width: '100%',
              borderRadius: 10,
            }}
          />
        </View>
        <View
          style={{
            flex: 1,
            position: 'absolute',
            height: '90%',
            width: '100%',
            justifyContent: 'flex-end',
          }}>
          <View>
            <View
              style={{
                backgroundColor: 'transparent',
                borderRadius: 10,
                margin: 40,
                marginBottom: 10,
              }}>
              <Text
                style={{
                  color: '#FFFFFF',
                  textAlign: 'center',
                  fontFamily: 'Roboto-Medium',
                  fontSize: 15,
                }}>
                {' Pindai QR atau Barcode \n untuk memulai melakukan Ujian'}
              </Text>
            </View>
            <View
              style={{
                alignItems: 'center',
                width: '100%',
                marginVertical: 10,
              }}>
              <TouchableOpacity onPress={() => this.togleFlash()}>
                {this.state.flashOn === true ? <SVGTorchOff /> : <SVGTorch />}
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  cameraView: {
    flex: 1,
    justifyContent: 'flex-start',
  },
  maskOutter: {
    alignItems: 'center',
    height: '100%',
    justifyContent: 'space-around',
    left: 0,
    position: 'absolute',
    top: 0,
    width: '100%',
  },
  containerLoading: {
    alignItems: 'center',
    backgroundColor: 'rgba(192,192,192,0.3)',
    height: '100%',
    justifyContent: 'center',
    position: 'absolute',
    width: '100%',
    zIndex: 2,
  },
  maskInner: {
    alignItems: 'center',
    backgroundColor: 'transparent',
    height: 301,
    justifyContent: 'center',
    width: 300,
  },
  rectangleContainer: {
    alignItems: 'center',
    backgroundColor: 'transparent',
    flex: 1,
    justifyContent: 'center',
  },
  maskFrame: {
    backgroundColor: 'rgba(0,0,0,0.6)',
  },
  maskRow: {
    width: '100%',
  },
  maskCenter: {flexDirection: 'row'},
  scanBar: {
    backgroundColor: '#22ff00',
    height: 1,
    width: 270,
  },
});

export default Camera;
