import React, {Component} from 'react';
import {View, AppState, BackHandler, ActivityIndicator} from 'react-native';
import {WebView} from 'react-native-webview';
import ModalLogout from '../components/ModalLogout';

class Detail extends Component {
  constructor(props) {
    super(props);
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    this.state = {
      data: props.navigation.state.params.ScanDetail,
      logoutVisible: false,
      appState: AppState.currentState,
    };
  }

  componentDidMount() {
    BackHandler.addEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
    AppState.addEventListener('change', this._handleAppStateChange);
  }

  UNSAFE_componentWillUnmount() {
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
    AppState.removeEventListener('change', this._handleAppStateChange);
  }

  _handleAppStateChange = nextAppState => {
    if (
      this.state.appState.match(/inactive|background/) &&
      nextAppState === 'active'
    ) {
      BackHandler.exitApp();
    }
    this.setState({appState: nextAppState});
  };

  handleBackButtonClick = () => {
    this.setState({logoutVisible: true});
    return true;
  };

  closeModalLogout = () => {
    this.setState({logoutVisible: false});
  };

  logout = async () => {
    BackHandler.exitApp();
  };

  ActivityIndicatorLoadingView() {
    //making a view to show to while loading the webpage
    return (
      <ActivityIndicator
        color="red"
        size="large"
        style={{
          position: 'absolute',
          left: 0,
          right: 0,
          top: 0,
          bottom: 0,
          alignItems: 'center',
          justifyContent: 'center',
        }}
      />
    );
  }

  render() {
    const {data, logoutVisible} = this.state;
    return (
      <>
        <ModalLogout
          title="Keluar ujian"
          desc="Apakah anda yakin ingin keluar ujian ?"
          txtCancel="Batal"
          txtConfirm="Keluar"
          onVisible={logoutVisible}
          onCancel={this.closeModalLogout}
          onConfirm={this.logout}
        />
        <WebView
          source={{uri: data}}
          style={{marginTop: 20}}
          javaScriptEnabled={true}
          domStorageEnabled={true}
          renderLoading={this.ActivityIndicatorLoadingView}
          startInLoadingState={true}
        />
      </>
    );
  }
}
export default Detail;
