import React from 'react';
import {StyleSheet, Text, Modal, View, TouchableOpacity} from 'react-native';

const ModalLogout = ({
  onVisible,
  type,
  onCancel,
  onConfirm,
  title,
  desc,
  txtCancel,
  txtConfirm,
}) => (
  <Modal
    animationType="slide"
    visible={onVisible}
    onRequestClose={() => {
      true;
    }}
    transparent
    supportedOrientations={['portrait', 'landscape']}
    style={styles.modal}>
    <View style={styles.viewBackgroundModal}>
      <View style={styles.modalContainer}>
        <View style={styles.textWrapper}>
          <View>
            <Text style={styles.textStyle}>{title}</Text>
            <Text style={styles.txt2}>{desc}</Text>
          </View>
        </View>
        <View style={styles.buttonWrapper}>
          <TouchableOpacity onPress={onCancel}>
            <View
              style={{
                backgroundColor: '#EE3B49',
                marginHorizontal: 8,
                alignItems: 'center',
                justifyContent: 'center',
                borderRadius: 30,
                height: 40,
                width: 100,
              }}>
              <Text style={{color: 'white', fontSize: 18}}>{txtCancel}</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={onConfirm}>
            <View
              style={{
                borderWidth: 1,
                borderColor: '#EE3B49',
                alignItems: 'center',
                justifyContent: 'center',
                borderRadius: 30,
                height: 40,
                width: 100,
              }}>
              <Text style={{color: '#EE3B49', fontSize: 18}}>{txtConfirm}</Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  </Modal>
);

export default ModalLogout;

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    alignSelf: 'center',
  },
  viewBackgroundModal: {
    flex: 1,
    alignSelf: 'stretch',
    justifyContent: 'center',
    backgroundColor: 'rgba(0,0,0,0.8)',
    padding: 30,
  },
  modalContainer: {
    textAlign: 'center',
    backgroundColor: '#fff',
    borderRadius: 5,
    padding: 30,
  },
  textWrapper: {
    padding: 5,
    marginBottom: 10,
  },
  textStyle: {
    fontSize: 18,
    fontWeight: '500',
    color: 'black',
    lineHeight: 25,
  },
  textRed: {
    fontSize: 20,
    fontWeight: '400',
    color: 'red',
    lineHeight: 25,
  },
  textType: {
    color: 'red',
    textTransform: 'uppercase',
    fontWeight: 'bold',
  },
  buttonWrapper: {
    marginTop: 20,
  },
  txt2: {
    paddingTop: 15,
    fontSize: 16,
    fontWeight: '400',
    color: 'black',
    lineHeight: 25,
  },
  buttonWrapper: {
    flexDirection: 'row',
    alignSelf: 'flex-end',
  },
});
