import React from 'react';
import Svg, { Circle, Path } from 'react-native-svg';
/* SVGR has dropped some elements not supported by react-native-svg: style */

const SvgAccount = props => {
  return (
    <Svg
      width="45"
      height="45"
      viewBox="0 0 45 45"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Circle cx="22.5" cy="22.5" r="22" stroke="white" />
      <Path
        fill-rule="evenodd"
        clip-rule="evenodd"
        d="M21.4894 21.1084C21.3521 21.1013 21.2628 20.9609 21.3145 20.8335L25.3621 10.8652C25.4469 10.6563 25.1749 10.4885 25.0263 10.6582L13.2204 24.1363C13.1101 24.2623 13.1942 24.46 13.3615 24.4679L21.3363 24.8421C21.4743 24.8486 21.5643 24.9897 21.512 25.1176L17.7376 34.3453C17.6543 34.549 17.9131 34.7183 18.0663 34.5603L30.345 21.9019C30.4646 21.7786 30.3834 21.5719 30.2118 21.563L21.4894 21.1084Z"
        fill="white"
      />
    </Svg>
  );
};

export default SvgAccount;
