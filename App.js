import {createAppContainer, createStackNavigator} from 'react-navigation';

import Home from './src/screens/Home';
import Camera from './src/screens/Camera';
import Detail from './src/screens/Detail';

const AppNavigator = createStackNavigator(
  {
    Home: {
      screen: Home,
      navigationOptions: {
        title: 'Home',
        header: null,
      },
    },
    Camera: {
      screen: Camera,
      navigationOptions: {
        title: 'Scan QR',
        header: null,
      },
    },
    Detail: {
      screen: Detail,
      navigationOptions: {
        title: 'ExCode Test',
        headerLeft: null,
      },
    },
  },
  {
    initialRouteName: 'Home',
  },
);

export default createAppContainer(AppNavigator);
